package spring.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;

import spring.model.IpAddress;

/**
 * Handles requests for the Employee service.
 */
@Controller
public class IpController {
	
	
	@RequestMapping(value = "/northcountries", method = RequestMethod.GET)
	public @ResponseBody IpAddress getIp(@RequestParam String[] ip) {
		IpAddress ipAddress = new IpAddress();
		Set<String> adresses = new TreeSet<String>();
		for (int i =0; i<ip.length;i++) {
			RestTemplate restTemplate = new RestTemplate();
			String url = "https://ipvigilante.com/json/{id}/country_name";
			Map<String, String> params = new HashMap<String, String>();
			params.put("id", ip[i]);
			String result = restTemplate.getForObject(url, String.class, params);
			String country = result.split("country_name")[1].replaceAll("[\\[\\]\"]", "").replaceAll(":", "")
					.replaceAll("}", "");
			adresses.add(country);
		}
		ipAddress.setNorthcountries(adresses);

	
		return ipAddress;
	    		  
	}

}
